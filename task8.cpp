/*
 * Task 8: Написати за допомогою стандарту OpenMP програму,
 *         яка визначає номери потоків.
 */

#include <iostream>

#include <omp.h>


int main(int argc, char* argv[])
{
    #pragma omp parallel
    {
        #pragma omp critical
        std::cout << "Thread " << omp_get_thread_num() << std::endl;
    }

    return 0;
}
