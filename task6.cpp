/*
 * Task 6: Реалізувати на основі технології MPI програму розподілу
 *         даних по процесорах.
 *
 * Example: parallel search of the max element in a vector. The vector
 *          is filled with random numbers and splitted (`scattered`)
 *          among several program instances. Each instance finds the
 *          max element in its subvector and returns it to the master
 *          process (`gathering`). The master process chooses the max
 *          element of all gathered max elements of subvectors.
 */

#include <vector>
#include <algorithm>
#include <iostream>

#include <mpi.h>


int rand1000()
{
    return rand() % 1000;
}

int master(int comm_size, int vector_size)
{
    std::vector<int> v;
    srand(time(0));
    std::generate_n(std::back_inserter(v), vector_size, rand1000);

    int elements_for_each = (comm_size > 1)? v.size() / (comm_size - 1) : 0;
    int elements_for_master = (comm_size > 1)? v.size() % (comm_size - 1) : v.size();

    for (int i = 0; i < comm_size - 1; ++i)
        MPI_Send(&v.front() + i * elements_for_each, elements_for_each, MPI_INT, i + 1, 0, MPI_COMM_WORLD);

    std::vector<int> maxs;
    if (elements_for_master > 0)
        maxs.push_back(*std::max_element(&v.back() - elements_for_master, &v.back()));

    for (int i = 1; i < comm_size; ++i)
    {
        MPI_Status status;
        int buf;
        MPI_Recv(&buf, 1, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);

        maxs.push_back(buf);
    }

    return *std::max_element(maxs.begin(), maxs.end());
}

void slave(int rank)
{
    const int MAX_BUF_SIZE = 1024 * 32;
    std::vector<int> buf(MAX_BUF_SIZE);

    MPI_Status status;

    MPI_Recv(&buf.front(), MAX_BUF_SIZE, MPI_INT, 0, 0, MPI_COMM_WORLD, &status);

    int count;
    MPI_Get_count(&status, MPI_INT, &count);

    int max = *std::max_element(&buf.front(), &buf.front() + count);
    MPI_Send(&max, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
}

int main(int argc, char* argv[])
{
    int vector_size = (argc > 1)? atoi(argv[1]) : 1024;

    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0)
        std::cout << "Max element: " << master(size, vector_size) << std::endl;
    else
        slave(rank);

    MPI_Finalize();
    return 0;
}
