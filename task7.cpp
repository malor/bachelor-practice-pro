/*
 * Task 7: Написати за допомогою стандарту OpenMP програму множення матриць.
 */

#include <vector>
#include <algorithm>
#include <iostream>


int rand10()
{
    return rand() % 10;
}

void print_square_matrix(const std::vector<int>& m, int n, const std::string& title = "Matrix")
{
    std::cout << title << ": " << std::endl;
    for (int i = 0; i < n; ++i)
    {
        for (int j = 0; j < n; ++j)
            std::cout << m[i * n + j] << " ";
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main(int argc, char* argv[])
{
    // for the sake of brevity let's mutliply only square matrices
    // matrix size `n` is passed as a first command line argument
    int n = atoi(argv[1]);

    std::vector<int> m1, m2, m3(n * n);

    srand(time(0));
    std::generate_n(std::back_inserter(m1), n * n, rand10);
    std::generate_n(std::back_inserter(m2), n * n, rand10);

    #pragma omp parallel for
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            for (int k = 0; k < n; ++k)
                m3[i * n + j] += m1[i * n + k] * m2[k * n + j];

    print_square_matrix(m1, n, "A");
    print_square_matrix(m2, n, "B");
    print_square_matrix(m3, n, "C");

    return 0;
}
