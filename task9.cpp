/*
 * Task 9: Написати за допомогою стандарту OpenMP програму
 * скалярного множення двох векторів.
 */

#include <vector>
#include <algorithm>
#include <iterator>
#include <cstdlib>
#include <iostream>


int rand10()
{
    return rand() % 10;
}

int main(int argc, char* argv[])
{
    // vector size is passed as a first command line argument
    int n = atoi(argv[1]);

    std::vector<int> v1, v2, res(n);

    srand(time(0));
    std::generate_n(std::back_inserter(v1), n, rand10);
    std::generate_n(std::back_inserter(v2), n, rand10);

    #pragma omp parallel for
    for (size_t i = 0; i < v1.size(); ++i)
        res[i] = v1[i] * v2[i];

    std::cout << "v1: ";
    std::copy(v1.begin(), v1.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::cout << "v2: ";
    std::copy(v2.begin(), v2.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    std::cout << "Result: ";
    std::copy(res.begin(), res.end(),
              std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;

    return 0;
}
